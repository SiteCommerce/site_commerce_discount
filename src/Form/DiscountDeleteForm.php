<?php

namespace Drupal\site_commerce_discount\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Discount entities.
 *
 * @ingroup site_commerce_discount
 */
class DiscountDeleteForm extends ContentEntityDeleteForm {

}

<?php

namespace Drupal\site_commerce_discount\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

use Drupal\user\UserInterface;

/**
 * Defines the Discount entity.
 *
 * @ingroup site_commerce_discount
 *
 * @ContentEntityType(
 *   id = "site_commerce_discount",
 *   label = @Translation("Discount"),
 *   label_collection = @Translation("Discounts"),
 *   label_singular = @Translation("discount"),
 *   label_plural = @Translation("discounts"),
 *   handlers = {
 *     "list_builder" = "Drupal\site_commerce_discount\DiscountListBuilder",
 *     "views_data" = "Drupal\site_commerce_discount\DiscountViewsData",
 *     "access" = "Drupal\site_commerce_discount\DiscountAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_discount\Form\DiscountForm",
 *       "add" = "Drupal\site_commerce_discount\Form\DiscountForm",
 *       "edit" = "Drupal\site_commerce_discount\Form\DiscountForm",
 *       "delete" = "Drupal\site_commerce_discount\Form\DiscountDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_discount\DiscountHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_discount",
 *   data_table = "site_commerce_discount_field_data",
 *   admin_permission = "administer site_commerce_discount",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "discount_id",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "owner" = "owner",
 *     "label" = "title",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/discount/{site_commerce_discount}",
 *     "add-form" = "/admin/site-commerce/config/discounts/add",
 *     "edit-form" = "/admin/site-commerce/config/discounts/{site_commerce_discount}/edit",
 *     "delete-form" = "/admin/site-commerce/config/discounts/{site_commerce_discount}/delete",
 *     "collection" = "/admin/site-commerce/config/discounts",
 *   },
 *   field_ui_base_route = "site_commerce_discount.settings",
 *   common_reference_target = TRUE
 * )
 */
class Discount extends ContentEntityBase implements DiscountInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
      'owner' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDiscountPluginId() {
    return $this->get('discount')->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('owner')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('owner')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('owner', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('owner', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setDescription(t('The user who created the discount in the system.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('A user in the system who can manage the discount settings.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Discount name'))
      ->setDescription(t('The name that is displayed in the administrative interface and on the promotion page for the current discount.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Discount description'))
      ->setDescription(t('Brief terms of the discount. Terms of the promotion for the current discount.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 1,
        'settings' => [
          'rows' => 5,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 1,
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['stores'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Stores'))
      ->setDescription(t('The stores for which the discount is valid.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE)
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['offer'] = BaseFieldDefinition::create('site_commerce_discount_plugin_item:site_commerce_discount_offer')
      ->setLabel(t('The method of calculating discount'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'kvantstudio_plugin_select_widget',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['rule_activation'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('The activation rule'))
      ->setDescription(t('Select the rule that the discount will be based on. By default, the discount is disabled and is not taken into account when calculating the cost.'))
      ->setSettings([
        'allowed_values' => [
          'disable' => t('No effect'),
          'always' => t('It always works'),
          'date' => t('Valid from the start date to the end date'),
        ],
      ])
      ->setDefaultValue('disable')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['date_start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Start date and time'))
      ->setRequired(TRUE)
      ->setDefaultValueCallback(static::class . '::getRequestTime')
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_stop'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Stop date and time'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'settings' => [
          'format_type' => 'short',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['usage_limit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Usage limit'))
      ->setDescription(t('The maximum number of times the promotion can be used. Input 0 for unlimited.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['usage_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of times used'))
      ->setDescription(t('The value in this field must be greater than zero and less than the usage limit value.'))
      ->setDefaultValue(0)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['compatibility'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Compatibility with other discounts'))
      ->setSetting('allowed_values_function', ['\Drupal\site_commerce_discount\Entity\Discount', 'getCompatibilityOptions'])
      ->setRequired(TRUE)
      ->setDefaultValue(self::COMPATIBLE_ANY)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('If this option is selected, the promotion page for the current discount will be available.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 8,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of the discount relative to other discounts. The higher the discount, the earlier it is applied when calculating the cost.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('Date the discount was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Date the discount was edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getRequestTime() {
    return \Drupal::time()->getRequestTime();
  }

  /**
   * Gets the allowed values for the 'compatibility' base field.
   *
   * @return array
   *   The allowed values.
   */
  public static function getCompatibilityOptions() {
    return [
      self::COMPATIBLE_ANY => t('Allow with other discounts'),
      self::COMPATIBLE_NONE => t('Not with any other discounts'),
    ];
  }
}
